<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert([
            [
                'identification_number' => '1122783238',
                'first_name'=>'Jhon',
                'second_name'=>'Frey',
                'first_lastname'=>'Diaz',
                'second_lastname'=>'Dejoy',
            ],
            [
                'identification_number' => '1122666222',
                'first_name'=>'Diana',
                'second_name'=>'Marcela',
                'first_lastname'=>'Armero',
                'second_lastname'=>'Gomez',
            ]
        ]);
    }
}
