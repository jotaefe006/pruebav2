<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Book extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'books';

    protected $fillable = [
        'identification_number',
        'first_name',
        'second_name',
        'first_lastname',
        'second_lastname'
    ];

    protected $hidden = [
        'created_at','updated_at','deleted_at'
    ];
}
