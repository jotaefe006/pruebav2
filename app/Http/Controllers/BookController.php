<?php

namespace App\Http\Controllers;

use App\Http\Requests\BookRequest;
use App\Http\Requests\BookUpdateRequest;
use Illuminate\Http\Request;
use App\Book;

class BookController extends Controller
{
    /**
     * GET Listar registros
     */
    public function index()
    {
        $books = Book::get();
        return $books;
    }

    /**
     * Post Insertar datos
     */
    public function store(BookRequest $request)
    {
        $input = $request->all();

        Book::create($input);

        return response()->json([
            'res' => true,
            'message' => 'Registro exitoso'
        ], 200);
    }

    /**
     * GET Obtiene un registro
     */
    public function show(Request $request)
    {
       
        $book = Book::where('id',$request->id)->get();
        return $book;
    }

    /**
     * PUT actualiza un registro
     */
    public function update(BookUpdateRequest $request, $id)
    {
       
        $book = Book::find($id);

        $book->identification_number = $request->identification_number;
        $book->first_name = $request->first_name;
        $book->second_name = $request->second_name;
        $book->first_lastname = $request->first_lastname;
        $book->second_lastname = $request->second_lastname;
        return $book;
    }

    /**
     * DELETE Elimina un registro
     */
    public function destroy(Request $request)
    {
        $book = Book::find($request->id);
        $book->delete();

        return response()->json([
            'res' => true,
            'message' => 'Registro eliminado'
        ], 200);
    }
}
